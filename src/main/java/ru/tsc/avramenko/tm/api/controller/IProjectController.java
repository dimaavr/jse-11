package ru.tsc.avramenko.tm.api.controller;

import ru.tsc.avramenko.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void showFindProject(Project project);

    void clearProjects();

    void createProject();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

}
