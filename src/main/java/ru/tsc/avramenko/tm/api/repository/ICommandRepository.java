package ru.tsc.avramenko.tm.api.repository;

import ru.tsc.avramenko.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
