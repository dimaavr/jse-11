package ru.tsc.avramenko.tm.controller;

import ru.tsc.avramenko.tm.api.controller.IProjectController;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        showFindProject(project);
    }

    @Override
    public void showFindProject(Project project) {
        System.out.println(project);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) System.out.println("!!!INCORRECT VALUES!!!");
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) System.out.println("!!!INCORRECT VALUES!!!");
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        projectService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        projectService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("!!!INCORRECT VALUES!!!");
            return;
        }
        projectService.removeByIndex(index);
        System.out.println("[OK]");
    }

}
